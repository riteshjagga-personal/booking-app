'use strict';

module.exports = function (app, cb) {

  var AppUser = app.models.AppUser;

  console.log('\n');
  console.log('-------------------------');
  console.log('         API Test        ');
  console.log('-------------------------');

  var email = 'ap-user1@yopmail.com';
  var startDate = new Date();
  var startHour = startDate.getHours() - 24;
  startDate.setHours(startHour);
  var endDate = null;
  console.log('Finding bookings for user:');
  console.log('Email: ' + email);
  console.log('Duration: ' + startDate + ':' + endDate);

  AppUser.findByEmail(email)
    .then(user => AppUser.getBookings(user.id, startDate, endDate, cb));

};
