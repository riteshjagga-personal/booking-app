'use strict';

module.exports = function (app, cb) {

  console.log('\n');
  console.log('-------------------------');
  console.log('        ADD ROLES        ');
  console.log('-------------------------');
  console.log('Adding roles...');

  var Role = app.models.Role;

  Role.count(function (error, count) {
    if (error) {
      cb(error);
    } else {
      if (count === 0) {
        console.log('Roles are not available');
        console.log('Creating roles...');
        Role.create([
          {name: 'super'},
          {name: 'administrator'},
          {name: 'employee'}
        ], function (error, roles) {
          if (error) throw error;

          console.log(roles.length + ' roles created');
          cb();
        });
      } else {
        console.log('Roles are already available');
        cb();
      }
    }
  });

};

