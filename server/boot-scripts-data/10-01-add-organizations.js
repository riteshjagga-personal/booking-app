'use strict';

module.exports = function (app, cb) {

  console.log('\n');
  console.log('-------------------------');
  console.log('     ADD ORGANIZATIONS   ');
  console.log('-------------------------');
  console.log('Adding organizations...');

  var Organization = app.models.Organization;

  Organization.count(function (error, count) {
    if (error) {
      cb(error);
    } else {
      if (count === 0) {
        console.log('Organizations are not available');
        console.log('Creating organizations...');

        Organization.create([{
          name: 'AnthroPower Training Pvt. Ltd.'
        }, {
          name: 'ManpraX Software LLP'
        }, {
          name: 'Beebom'
        }], function (error, organizations) {
          if (error) {
            cb(error);
          } else {
            console.log(organizations.length + ' organizations created');
            cb(error, organizations);
          }
        });

      } else {
        console.log('Organizations are already available');
        cb(error, count);
      }
    }
  });
};
