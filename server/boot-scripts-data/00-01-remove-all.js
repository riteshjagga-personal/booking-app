'use strict';

module.exports = function (app, cb) {

  console.log('-------------------------');
  console.log('      DELETE RECORDS     ');
  console.log('-------------------------');
  console.log('Deleting all records...');

  var AppUser = app.models.AppUser;
  var RoleMapping = app.models.RoleMapping;
  var Role = app.models.Role;
  var Hub = app.models.Hub;
  var Organization = app.models.Organization;
  var OrganizationHubTeam = app.models.OrganizationHubTeam;
  var Booking = app.models.Booking;

  var total = 7;
  var counter = 0;

  function check () {
    counter++;
    if (counter === total) {
      cb();
    }
  }

  AppUser.deleteAll(function (error, result) {
    if (error) {
      cb(error);
    } else {
      console.log('All users are deleted');
      check();
    }
  });

  RoleMapping.deleteAll(function (error, result) {
    if (error) {
      cb(error);
    } else {
      console.log('All role mappings are deleted');
      check();
    }
  });

  Role.deleteAll(function (error, result) {
    if (error) {
      cb(error);
    } else {
      console.log('All roles are deleted');
      check();
    }
  });

  Organization.deleteAll(function (error, result) {
    if (error) {
      cb(error);
    } else {
      console.log('All users are deleted');
      check();
    }
  });

  OrganizationHubTeam.deleteAll(function (error, result) {
    if (error) {
      cb(error);
    } else {
      console.log('All organizations are deleted');
      check();
    }
  });

  Hub.deleteAll(function (error, result) {
    if (error) {
      cb(error);
    } else {
      console.log('All countries are deleted');
      check();
    }
  });

  Booking.deleteAll(function (error, result) {
    if (error) {
      cb(error);
    } else {
      console.log('All currencies are deleted');
      check();
    }
  });

};
