'use strict';

module.exports = function (app, cb) {

  console.log('\n');
  console.log('-------------------------');
  console.log('      END OF SCRIPTS     ');
  console.log('-------------------------');
  console.log('\n');

  cb();

};
