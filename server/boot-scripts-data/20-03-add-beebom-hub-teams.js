'use strict';

var Promise = require('bluebird');

module.exports = function (app, cb) {

  console.log('\n');
  console.log('------------------------------------');
  console.log('     ADD ORGANIZATION HUB TEAMS     ');
  console.log('------------------------------------');

  var organizationName = 'Beebom';
  var cities = ['Delhi', 'Bangalore'];
  console.log('Adding organization hub teams for ' + organizationName + ' in hubs of ' + cities.toString() + ' cities...');

  var Hub = app.models.Hub;
  var Organization = app.models.Organization;
  var OrganizationHubTeam = app.models.OrganizationHubTeam;

  var error = null;

  Promise.all([
    Organization.findOne({where: {name: organizationName}}),
    Hub.find({where: {city: {inq: cities}}})
  ])
    .then(function (results) {

      var organization = results[0];
      var hubs = results[1];

      if (!organization) {
        error = new Error();
        error.message = 'Organization is not found';
        error.statusCode = 404;
        cb(error);
        console.log(error.message);
      } else if (hubs.length === 0) {
        error = new Error();
        error.message = 'Hubs are not found';
        error.statusCode = 404;
        cb(error);
      } else {

        var teams = hubs.map(function (hub) {
          return {
            hubId: hub.id,
            organizationId: organization.id
          }
        });

        OrganizationHubTeam.create(teams, function (error, teams) {
          if (error) {
            cb(error);
          } else {
            console.log(teams.length + ' hub teams created');
            cb(error, teams);
          }
        });

      }

    })
    .catch(function (error) {
      cb(error);
    });

};
