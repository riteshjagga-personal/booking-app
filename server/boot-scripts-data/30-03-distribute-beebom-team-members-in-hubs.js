'use strict';

var Promise = require('bluebird');

module.exports = function (app, cb) {

  console.log('\n');
  console.log('------------------------------');
  console.log('   DISTRIBUTE USERS IN TEAMS  ');
  console.log('------------------------------');

  var organizationName = 'ManpraX Software LLP';
  var cities = ['Delhi', 'Bangalore'];
  console.log('Distributing users of ' + organizationName + ' in hubs of ' + cities.toString() + ' cities...');

  var Hub = app.models.Hub;
  var Organization = app.models.Organization;
  var AppUser = app.models.AppUser;
  var OrganizationHubTeam = app.models.OrganizationHubTeam;

  var error = null;

  Promise.all([
    Organization.findOne({where: {name: organizationName}}),
    Hub.find({where: {city: {inq: cities}}})
  ])
    .then(function (results) {
      var organization = results[0];
      var hubs = results[1];

      if (!organization) {
        error = new Error();
        error.message = 'Organization is not found';
        error.statusCode = 404;
        cb(error);
        console.log(error.message);
      } else if (hubs.length === 0) {
        error = new Error();
        error.message = 'Hubs are not found';
        error.statusCode = 404;
        cb(error);
      } else {

        var hubsId = hubs.map(function (hub) {
          return hub.id
        });

        OrganizationHubTeam.find({
          where: {
            organizationId: organization.id,
            hubId: {inq: hubsId}
          }
        })
          .then(function (teams) {

            AppUser.find({
              where: {
                organizationId: organization.id
              },
              order: 'firstName ASC'
            }, function (error, users) {
              if (error) {
                cb(error);
              } else if (users.length === 0) {
                error = new Error();
                error.message = 'Users are not found';
                error.statusCode = 404;
                cb(error);
              } else {

                var totalUsers = users.length;
                console.log(totalUsers + ' users found in ' + organizationName);

                // Divide users to both the hub teams
                var divideAtIndex = Math.floor(totalUsers / 2);

                var firstTeamUsers = users.slice(0, divideAtIndex);
                var secondTeamUsers = users.slice(divideAtIndex);

                console.log('Users divided at ' + divideAtIndex);

                var firstTeamUsersId = firstTeamUsers.map(function (user) {
                  return user.id;
                });

                var secondTeamUsersId = secondTeamUsers.map(function (user) {
                  return user.id;
                });

                // Commit them to datasource
                Promise.all([
                  AppUser.updateAll({id: {inq: firstTeamUsersId}}, {teamId: teams[0].id}),
                  AppUser.updateAll({id: {inq: secondTeamUsersId}}, {teamId: teams[1].id})
                ])
                  .then(function (results) {
                    console.log(organizationName + ' users team distributed in hub teams at ' + cities.toString() + ' cities');
                    cb(error, teams);
                  })
                  .catch(function (error) {
                    cb(error);
                  });

              }
            });

          })
          .catch(function (error) {
            cb(error);
          });

      }

    })
    .catch(function (error) {
      cb(error);
    });

};
