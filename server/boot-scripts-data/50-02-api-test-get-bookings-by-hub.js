'use strict';

module.exports = function (app, cb) {

  var Hub = app.models.Hub;

  console.log('\n');
  console.log('-------------------------');
  console.log('         API Test        ');
  console.log('-------------------------');

  var city = 'Delhi';
  var startDate = new Date();
  var startHour = startDate.getHours() - 24;
  startDate.setHours(startHour);
  var endDate = null;
  console.log('Finding bookings for all teams in hub:');
  console.log('City: ' + city);
  console.log('Duration: ' + startDate + ':' + endDate);

  Hub.findByCity(city)
    .then(hub => Hub.getBookings(hub.id, startDate, endDate, cb));
};
