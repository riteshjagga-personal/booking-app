'use strict';

module.exports = function (app, cb) {

  console.log('\n');
  console.log('-------------------------');
  console.log('         ADD HUBS        ');
  console.log('-------------------------');
  console.log('Adding hubs...');

  var Hub = app.models.Hub;

  Hub.count(function (error, count) {
    if (error) {
      cb(error);
    } else {
      if (count === 0) {
        console.log('Hubs are not available');
        console.log('Creating hubs...');

        Hub.create([{
          name: '91 Springboard - Bangalore',
          city: 'Bangalore'
        }, {
          name: '91 Springboard - Delhi',
          city: 'Delhi'
        }, {
          name: '91 Springboard - Gurgaon',
          city: 'Gurgaon'
        }, {
          name: '91 Springboard - Hyderabad',
          city: 'Hyderabad'
        }, {
          name: '91 Springboard - Mumbai',
          city: 'Mumbai'
        }, {
          name: '91 Springboard - Navi Mumbai',
          city: 'Navi Mumbai'
        }, {
          name: '91 Springboard - Noida',
          city: 'Noida'
        }], function (error, hubs) {
          if (error) {
            cb(error);
          } else {
            console.log(hubs.length + ' hubs created');
            cb(error, hubs);
          }
        });

      } else {
        console.log('Hubs are already available');
        cb(error, count);
      }
    }
  });
};
