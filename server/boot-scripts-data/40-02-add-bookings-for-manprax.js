'use strict';

var Util = require('./../scripts/util');

module.exports = function (app, cb) {

  console.log('\n');
  console.log('------------------------------');
  console.log('         ADD BOOKINGS         ');
  console.log('------------------------------');

  var organizationName = 'ManpraX Software LLP';
  console.log('Adding bookings for ' + organizationName);

  var Organization = app.models.Organization;
  var AppUser = app.models.AppUser;
  var Booking = app.models.Booking;

  Organization.findOne({
    where: {
      name: organizationName
    }
  }, function (error, organization) {
    if (error) {
      cb(error);
    } else if (!organization) {
      console.log('Organization is not found');
      cb(error, organization);
    } else {
      console.log('Organization found having id ' + organization.id);

      AppUser.find({
        where: {
          organizationId: organization.id
        }
      }, function (error, users) {
        if (error) {
          cb(error);
        } else if (users.length === 0) {
          error = new Error();
          error.message = 'Users are not found';
          error.statusCode = 404;
          cb(error, users);
        } else {

          var totalUsers = users.length;
          console.log(totalUsers + ' users found in ' + organizationName);
          var user = null;
          var bookings = [];
          var totalBookingsPerUser = 10;

          for (var i = 0; i < totalUsers; i++) {
            user = users[i];

            for (var j = 0; j < totalBookingsPerUser; j++) {
              var startHour = j;
              var endHour = startHour + 1;

              var startDate = new Date();
              startDate.setHours(startHour);

              var endDate = new Date();
              endDate.setHours(endHour);

              var duration = Util.getDurationInMinutes(startDate, endDate);
              bookings.push({
                ownerId: user.id,
                startDate: startDate,
                endDate: endDate,
                duration: duration
              });
            }

          }

          // Commit them to datasource
          Booking.create(bookings, function (error, teams) {
            if (error) {
              cb(error);
            } else {
              console.log(totalBookingsPerUser + ' bookings created for all ' + totalUsers + ' users of ' + organizationName);
              console.log('A total of ' + bookings.length + ' bookings have been created');
              cb(error, teams);
            }
          });

        }
      });

    }
  });

};

