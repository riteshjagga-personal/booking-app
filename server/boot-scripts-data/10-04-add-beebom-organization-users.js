'use strict';

module.exports = function (app, cb) {

  console.log('\n');
  console.log('--------------------------');
  console.log('         ADD USERS        ');
  console.log('--------------------------');

  var organizationName = 'Beebom';
  console.log('Adding users in ' + organizationName);

  var Organization = app.models.Organization;
  var AppUser = app.models.AppUser;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;

  Organization.findOne({
    where: {
      name: organizationName
    }
  }, function (error, organization) {
    if (error) {
      cb(error);
    } else if (!organization) {
      console.log('Organization is not found');
      cb(error, organization);
    } else {
      console.log('Organization found having id ' + organization.id);

      var users = [];
      var user = null;
      var totalUsers = 10;
      for (var i = 0; i < totalUsers; i++) {
        user = {
          firstName: 'User' + (i + 1),
          lastName: '@ Beebom',
          email: 'beebom-user' + (i + 1) + '@yopmail.com',
          password: 'India@123',
          organizationId: organization.id
        };
        users.push(user);
      }

      AppUser.create(users, function (error, users) {
        if (error) {
          cb(error);
        } else {
          console.log(users.length + ' users created');
          console.log('Assigning employee role...');

          Role.byName('employee', function (error, role) {
            if (error) {
              cb(error);
            } else {
              var roleMappings = users.map(function (user) {
                return {
                  principalType: RoleMapping.USER,
                  principalId: user.id
                };
              });

              role.principals.create(roleMappings, function (error, principals) {
                if (error) {
                  cb(error);
                } else {
                  console.log('Employee role assigned to ' + principals.length + ' users');
                  cb();
                }
              });
            }
          });
        }
      });

    }
  });

};
