'use strict';

module.exports = function (app) {
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;
  var AppUser = app.models.AppUser;

  /**
   * Get a role by name
   * @param name
   */
  Role.byName = function (name, cb) {
    Role.findOne({
      where: {name: name}
    }, cb);
  };

  /**
   * Get user ID's by role name
   * @param role
   * @param cb
   */
  RoleMapping.getUsersIdByRole = function (role, cb) {
    var error = null;

    Role.byName(role, function (error, role) {
      if (error) {
        cb(error);
      } else if (!role) {
        error = new Error();
        error.statusCode = 404;
        error.message = 'Role is not found';
        cb(error);
      } else {
        RoleMapping.find({
          where: {
            roleId: role.id,
            principalType: RoleMapping.USER
          }
        }, function (error, principals) {
          if (error) {
            cb(error);
          } else {
            var users = principals.map(function (principal) {
              return principal.principalId;
            });

            cb(null, users);
          }
        });
      }
    });

  };

  AppUser.hasMany(Role, {through: RoleMapping, foreignKey: 'principalId'});
};

