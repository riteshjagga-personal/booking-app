'use strict';

module.exports = {
  padWithZero: padWithZero,
  getDurationInMinutes: getDurationInMinutes,
  groupBy: groupBy,
  getFirstAndLastDatesOfCurrentMonth: getFirstAndLastDatesOfCurrentMonth
};

function padWithZero (value) {
  if (value >= 0 && value < 10) {
    return '0' + value.toString();
  } else {
    return value.toString();
  }
}

function getDurationInMinutes (startDate, endDate) {
  var ONE_MINUTE_IN_MILLISECONDS = 60 * 1000;
  var difference = endDate.getTime() - startDate.getTime();
  return (difference / ONE_MINUTE_IN_MILLISECONDS);
}

function groupBy (sourceArray, groupByPropertyName, targetPropertyName) {
  var groupToValues = sourceArray.reduce(function (groupObject, item) {
    groupObject[item[groupByPropertyName]] = groupObject[item[groupByPropertyName]] || [];
    groupObject[item[groupByPropertyName]].push(item);
    return groupObject;
  }, {});

  var groups = Object.keys(groupToValues).map(function (key) {
    var obj = {};
    obj[groupByPropertyName] = key;
    obj[targetPropertyName] = groupToValues[key];
    return obj;
  });

  return groups;
}

function getFirstAndLastDatesOfCurrentMonth () {
  var current = new Date();
  var startDate = new Date(current.getFullYear(), current.getMonth(), 1);
  var endDate = new Date(current.getFullYear(), current.getMonth() + 1, 0);
  return {
    startDate: startDate,
    endDate: endDate
  }
}
