'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = function () {
  var LOCALHOST_PORT = 3000;
  var serverPort = process.env.PORT || LOCALHOST_PORT;
  console.log('Server Port: %s', serverPort);

  // start the web server
  return app.listen(serverPort, function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);

    // Set environment specific port for TCP (http/https)
    var environment = process.env.NODE_ENV;
    console.log('environment: %s', environment);
    var protocol = app.get('protocol');
    console.log('Protocol: ' + protocol);
    var port = LOCALHOST_PORT;
    if (environment === 'staging' || environment === 'production') {
      port = (protocol === 'https') ? 443 : 80;
    }
    app.set('port', port);
    console.log('TCP Port: %s', port);
    console.log('TCP Port using app.get("port"): %s', app.get('port'));


    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
var bootOptions = {
  appRootDir: __dirname,
  bootDirs: ['boot-scripts', 'boot-scripts-data']
};

boot(app, bootOptions, function (error) {
  if (error) throw error;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
