'use strict';

var Promise = require('bluebird');
var Util = require('./../scripts/Util');

module.exports = function (Booking) {

  Booking.getBookingsByHub = function (hubId, startDate, endDate, cb) {

    var OrganizationHubTeam = Booking.app.models.OrganizationHubTeam;
    var AppUser = Booking.app.models.AppUser;
    var Hub = Booking.app.models.Hub;

    var error = null;

    // Get Hub Teams
    OrganizationHubTeam.find({
      where: {
        hubId: hubId
      }
    }, function (error, teams) {
      if (error) {
        cb(error);
      } else {

        console.log(teams.length + ' teams found in the hub.');

        /*var teamsId = teams.map(function (team) {
         return team.id;
         });*/

        var teamsId = teams.map(function (team) {
          return team.id;
        });

        // Get all users of all hub teams
        AppUser.find({
          where: {
            teamId: {inq: teamsId}
          }
        }, function (error, users) {
          if (error) {
            cb(error);
          } else {
            console.log(users.length + ' users found in all the teams of the hub.');

            var usersId = users.map(function (user) {
              return user.id;
            });

            console.log('Getting bookings for ' + usersId.length + ' users and duration from ' + startDate + ' to ' + endDate);
            if (startDate) {
              console.log('Start Date (ISO Format): ' + startDate.toISOString());
            }
            if (endDate) {
              console.log('End Date (ISO Format): ' + endDate.toISOString());
            }

            /* Get all bookings of all users
             * Create filter and add where filter to filter results of all users id
             */
            var filter = getBookingsFilter(startDate, endDate);
            filter.where.ownerId = {inq: usersId};
            Booking.find(filter, function (error, bookings) {
              if (error) {
                cb(error);
              } else {
                var resultGroupedByTeam = {
                  items: [],
                  summary: null
                };

                var result = getBookingsResult(bookings);

                // Get summary
                resultGroupedByTeam.summary = result.summary;

                // Get bookings grouped by team
                console.log('Grouping bookings by team');
                var bookingsGroupedByTeam = Util.groupBy(result.bookings, 'teamId', 'bookings');
                resultGroupedByTeam.items = bookingsGroupedByTeam;

                cb(error, resultGroupedByTeam);
              }
            });

          }
        });

      }
    });
  };

  Booking.remoteMethod('getBookingsByHub', {
    accepts: [
      {arg: 'hubId', type: 'string', required: true},
      {arg: 'startDate', type: 'date', required: false},
      {arg: 'endDate', type: 'date', required: false}
    ],
    http: {path: '/getBookingsByHub', verb: 'get'},
    returns: {arg: 'bookingsResult', type: 'object'}
  });
  Booking.getBookingsByOwner = function (ownerId, startDate, endDate, cb) {

    console.log('Getting bookings for ' + ownerId + ' and duration from ' + startDate + ' to ' + endDate);
    if (startDate) {
      console.log('Start Date (ISO Format): ' + startDate.toISOString());
    }
    if (endDate) {
      console.log('End Date (ISO Format): ' + endDate.toISOString());
    }

    /* Get all bookings of a user
     * Create filter and add where filter to filter results of user having id equal to ownerId
     */
    var filter = getBookingsFilter(startDate, endDate);
    filter.where.ownerId = ownerId;
    Booking.find(filter, function (error, bookings) {
      if (error) {
        cb(error);
      } else {
        var result = getBookingsResult(bookings);
        cb(error, result);
      }
    });
  };

  Booking.remoteMethod('getBookingsByOwner', {
    accepts: [
      {arg: 'ownerId', type: 'string', required: true},
      {arg: 'startDate', type: 'date', required: false},
      {arg: 'endDate', type: 'date', required: false}
    ],
    http: {path: '/getBookingsByOwner', verb: 'get'},
    returns: {arg: 'bookingsResult', type: 'object'}
  });

  Booking.getBookingsSummaryByOwner = function (ownerId, startDate, endDate, cb) {

    console.log('Getting bookings for ' + ownerId + ' and duration from ' + startDate + ' to ' + endDate);
    if (startDate) {
      console.log('Start Date (ISO Format): ' + startDate.toISOString());
    }
    if (endDate) {
      console.log('End Date (ISO Format): ' + endDate.toISOString());
    }

    var summary = {
      totalMinutesBooked: 0
    };

    // Create filter
    var ownerObjectId = Booking.getDataSource().ObjectID(ownerId);
    var whereConditions = [{
      ownerId: ownerObjectId
    }];

    var range = undefinedNullDateCheck(startDate, endDate);

    if (range.startDate !== null) {
      whereConditions.push({startDate: {$gte: range.startDate}})
    }

    if (range.endDate !== null) {
      whereConditions.push({endDate: {$lte: range.endDate}})
    }

    console.log(whereConditions);

    // Booking.getDataSource().connector.connect
    Booking.getDataSource().connector.connect(function (error, db) {

      if (error) {
        cb(error);
      } else {

        var collection = db.collection('Booking');
        collection.aggregate([{
          $match: {
            $and: whereConditions
          }
        }, {
          $group: {
            _id: ownerObjectId,
            total: {$sum: "$duration"}
          }
        }], function (error, result) {
          if (error) {
            cb(error);
          } else {
            console.log(result[0]);
            summary.totalMinutesBooked = result[0].total;
            cb(null, summary);
          }
        });

      }

    });

  };
  Booking.remoteMethod('getBookingsSummaryByOwner', {
    accepts: [
      {arg: 'ownerId', type: 'string', required: true},
      {arg: 'startDate', type: 'date', required: false},
      {arg: 'endDate', type: 'date', required: false}
    ],
    http: {path: '/getBookingsSummaryByOwner', verb: 'get'},
    returns: {arg: 'bookingsResult', type: 'object'}
  });

  function getBookingsFilter (startDate, endDate) {

    var range = undefinedNullDateCheck(startDate, endDate);

    var whereFilter = {};
    if (range.startDate !== null) {
      whereFilter.startDate = {gte: range.startDate};
    }

    if (range.endDate !== null) {
      whereFilter.endDate = {lte: range.endDate};
    }

    var filter = {
      where: whereFilter,
      include: {
        relation: 'owner',
        scope: {
          fields: ['firstName', 'lastName', 'organizationId', 'teamId'],
          include: [{
            relation: 'organization',
            scope: {
              fields: ['name']
            }
          }, {
            relation: 'team',
            scope: {
              fields: ['hubId', 'organizationId']
            }
          }]
        }
      }
    };

    return filter;
  }

  function undefinedNullDateCheck (startDate, endDate) {
    var range = {
      startDate: null,
      endDate: null
    };

    /* Pptional parameters are set to undefined when a remote method is called
     * To prevent checking for both undefined and null cases
     * First, undefined is converted to null (absence of value) and
     * then null values are handled later in the code
     */
    if (startDate === undefined) {
      startDate = null;
    }

    if (endDate === undefined) {
      endDate = null;
    }

    /* When both startDate and endDate are null
     * Get current month and set startDate to the first and endDate to the last day of the month
     */
    if (startDate === null && endDate === null) {
      var current = new Date();
      startDate = new Date(current.getFullYear(), current.getMonth(), 1);
      endDate = new Date(current.getFullYear(), current.getMonth() + 1, 0);
    }

    return {
      startDate: startDate,
      endDate: endDate
    }
  }

  function getBookingsResult (bookings) {
    var result = {
      bookings: [],
      summary: {
        totalMinutesBooked: 0
      }
    };

    var totalMinutesBooked = 0;

    // Map nested data to the first level in bookings array
    bookings = bookings.map(function (booking) {

      var owner = booking.owner();
      var organization = owner.organization();
      var team = owner.team();

      totalMinutesBooked += booking.duration;

      return {
        id: booking.id,
        startDate: booking.startDate,
        endDate: booking.endDate,
        duration: booking.duration,
        ownerId: booking.ownerId,
        ownerName: owner.firstName + ' ' + owner.lastName,
        organizationName: organization.name,
        teamId: team.id,
        hubId: team.hubId,
        organizationId: team.organizationId
      }
    });

    result.bookings = bookings;
    result.summary.totalMinutesBooked = totalMinutesBooked;

    return result;
  }

  function getFirstAndLastDatesOfCurrentMonth () {
    var current = new Date();
    var startDate = new Date(current.getFullYear(), current.getMonth(), 1);
    var endDate = new Date(current.getFullYear(), current.getMonth() + 1, 0);
    return {
      startDate: startDate,
      endDate: endDate
    }
  }
};
