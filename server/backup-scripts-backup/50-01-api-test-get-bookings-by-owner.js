'use strict';

module.exports = function (app, cb) {

  var AppUser = app.models.AppUser;
  var Booking = app.models.Booking;
  var OrganizationHubTeam = app.models.OrganizationHubTeam;

  console.log('\n');
  console.log('-------------------------');
  console.log('         API Test        ');
  console.log('-------------------------');

  var email = 'ap-user1@yopmail.com';
  console.log('Finding bookings for user having email ' + email);

  AppUser.findOne({
    where: {
      email: email
    }
  }, function (error, user) {
    if (error) {
      cb(error);
    } else {
      console.log('User having email id ' + email + ' (' + user.id + ') found');

      //var startDate = null;
      var startDate = new Date();
      var startHour = startDate.getHours() + 2;
      startDate.setHours(startHour);

      var endDate = null;
      /*var endDate = new Date();
       var endHour = endDate.getHours() + 3;
       endDate.setHours(endHour);*/

      Booking.getBookingsByOwner(user.id, startDate, endDate, function (error, result) {
        if (error) {
          cb(error);
        } else {
          console.log(result);
          cb(error, result);
        }
      })
    }
  });

};
