'use strict';

module.exports = function (app, cb) {

  var AppUser = app.models.AppUser;
  var Booking = app.models.Booking;
  var OrganizationHubTeam = app.models.OrganizationHubTeam;
  var Hub = app.models.Hub;

  console.log('\n');
  console.log('-------------------------');
  console.log('         API Test        ');
  console.log('-------------------------');

  var city = 'Delhi';
  console.log('Finding bookings for all teams in hub of city ' + city);

  Hub.findOne({
    where: {
      city: city
    }
  }, function (error, hub) {
    if (error) {
      cb(error);
    } else {
      console.log('Hub in city ' + city + ' (' + hub.id + ') found');

      //var startDate = null;
      var startDate = new Date();
      var startHour = startDate.getHours() - 24;
      startDate.setHours(startHour);

      var endDate = null;
      /*var endDate = new Date();
       var endHour = endDate.getHours() + 3;
       endDate.setHours(endHour);*/

      Booking.getBookingsByHub(hub.id, startDate, endDate, function (error, result) {
        if (error) {
          cb(error);
        } else {
          console.log(result);
          if (result.items.length > 0) {
            console.log('Team Id: ' + result.items[0].teamId);
            console.log(result.items[0].bookings);
          }
          cb(error, result);
        }
      })
    }
  });

};
