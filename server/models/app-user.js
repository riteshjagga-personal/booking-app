'use strict';

var BookingService = require('./../models/booking-service');
var BookingQueryFilter = BookingService.BookingQueryFilter;
var BookingUserResponse = BookingService.BookingUserResponse;

module.exports = function (AppUser) {

  AppUser.findByEmail = function (email) {
    return AppUser.findOne({where: {email: email}});
  };

  AppUser.getBookingsByUsersId = function (usersId, startDate, endDate) {
    var Booking = AppUser.app.models.Booking;

    var current = new Date();
    startDate = startDate ? startDate : new Date(current.getFullYear(), current.getMonth(), 1);
    endDate = endDate ? endDate : new Date(current.getFullYear(), current.getMonth() + 1, 0);

    console.log('Users');
    console.log(usersId);
    console.log('Duration: ' + startDate.toISOString() + ':' + endDate.toISOString());

    /* Performance related:
     * If there is only one user id, get the user id out of array and apply where directly on user id
     * If there are multiple users id, apply where filter using inq operator on the usersId array to look all users id
     */
    var ownerIdFilterBy = (usersId.length === 1) ? usersId[0] : {inq: usersId};
    var filter = {
      where: {ownerId: ownerIdFilterBy, startDate: {gte: startDate}, endDate: {lte: endDate}},
      include: BookingQueryFilter.INCLUDE_FILTER
    };

    return Booking.find(filter);
  };

  AppUser.getBookings = function (userId, startDate, endDate, cb) {
    AppUser.getBookingsByUsersId([userId], startDate, endDate)
      .then(bookings => new BookingUserResponse(bookings))
      .then(response => {
        console.log(response);
        return response;
      })
      .then(response => cb(null, response))
      .catch(error => cb(error));
  };

  AppUser.remoteMethod('getBookings', {
    accepts: [
      {arg: 'userId', type: 'string', required: true},
      {arg: 'startDate', type: 'date', required: false},
      {arg: 'endDate', type: 'date', required: false}
    ],
    http: {path: '/getBookings', verb: 'get'},
    returns: {arg: 'bookingsResponse', type: 'object', root: true}
  });

};
