'use strict';

var Util = require('./../scripts/util');

class BookingQueryFilter {
  static get INCLUDE_FILTER () {
    return {
      relation: 'owner',
      scope: {
        fields: ['firstName', 'lastName', 'organizationId', 'teamId'],
        include: [{
          relation: 'organization',
          scope: {
            fields: ['name']
          }
        }, {
          relation: 'team',
          scope: {
            fields: ['hubId', 'organizationId']
          }
        }]
      }
    };
  }
}

class BookingItem {
  constructor (id, startDate, endDate, duration, ownerId, ownerName, organizationName, teamId, hubId, organizationId) {
    this.id = id;
    this.startDate = startDate;
    this.endDate = endDate;
    this.duration = duration;
    this.ownerId = ownerId;
    this.ownerName = ownerName;
    this.organizationName = organizationName;
    this.teamId = teamId;
    this.hubId = hubId;
    this.organizationId = organizationId
  }
}

class BookingResponse {
  constructor (rawBookings) {
    this.bookings = this.process(rawBookings);
    this.summary = {totalMinutesBooked: 0};
  }

  process (rawBookings) {
    return rawBookings.map(rawBooking => {
      var owner = rawBooking.owner();
      var organization = owner.organization();
      var team = owner.team();

      return new BookingItem(rawBooking.id,
        rawBooking.startDate,
        rawBooking.endDate,
        rawBooking.duration,
        rawBooking.ownerId,
        (owner.firstName + ' ' + owner.lastName),
        organization.name,
        team.id,
        team.hubId,
        team.organizationId);
    });
  }
}

class BookingUserResponse extends BookingResponse {
  constructor (rawBookings) {
    super(rawBookings);
    this.postProcess();
  }

  postProcess () {
    var totalMinutesBooked = this.bookings.reduce((totalMinutesBooked, booking) => {
      return (totalMinutesBooked + booking.duration);
    }, 0);

    this.summary.totalMinutesBooked = totalMinutesBooked;
  }
}

class BookingHubResponse extends BookingResponse {
  constructor (rawBookings) {
    super(rawBookings);
    this.postProcess();
  }

  postProcess () {
    var totalMinutesBooked = this.bookings.reduce((totalMinutesBooked, booking) => {
      return (totalMinutesBooked + booking.duration);
    }, 0);

    this.summary.totalMinutesBooked = totalMinutesBooked;

    // Group bookings by team id
    this.bookings = Util.groupBy(this.bookings, 'teamId', 'bookings');
  }
}

module.exports = {
  BookingQueryFilter: BookingQueryFilter,
  BookingUserResponse: BookingUserResponse,
  BookingHubResponse: BookingHubResponse
};
