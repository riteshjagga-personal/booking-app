'use strict';

var BookingService = require('./../models/booking-service');
var BookingHubResponse = BookingService.BookingHubResponse;

module.exports = function (Hub) {

  Hub.findByCity = function (city) {
    return Hub.findOne({where: {city: city}});
  };

  Hub.getTeams = function (hubId) {
    var OrganizationHubTeam = Hub.app.models.OrganizationHubTeam;
    return OrganizationHubTeam.find({where: {hubId: hubId}});
  };

  Hub.getUsersOfTeams = function (teamsId) {
    var AppUser = Hub.app.models.AppUser;
    return AppUser.find({where: {teamId: {inq: teamsId}}});
  };

  Hub.getBookings = function (hubId, startDate, endDate, cb) {
    var AppUser = Hub.app.models.AppUser;

    Hub.getTeams(hubId)
      .then(teams => teams.map(team => team.id)) // Create teams id array from teams array
      .then(teamsId => Hub.getUsersOfTeams(teamsId))
      .then(users => users.map(user => user.id)) // Create users id array from users array
      .then(usersId => AppUser.getBookingsByUsersId(usersId, startDate, endDate))
      .then(bookings => new BookingHubResponse(bookings))
      .then(response => {
        console.log(response);
        return response;
      })
      .then(response => cb(null, response))
      .catch(error => cb(error));
  };

  Hub.remoteMethod('getBookings', {
    accepts: [
      {arg: 'hubId', type: 'string', required: true},
      {arg: 'startDate', type: 'date', required: false},
      {arg: 'endDate', type: 'date', required: false}
    ],
    http: {path: '/getBookings', verb: 'get'},
    returns: {arg: 'bookingsResponse', type: 'object', root: true}
  });

};
