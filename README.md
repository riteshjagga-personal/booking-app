# Booking App
***

## 1. About
BookingApp is a coding challenge given by 91Springboard
to test code quality, clean architecture and software design.

Objective is to create 2 API endpoints to return meeting room bookings and
summary data in the following format:

````
{ bookings: [], summary: {} }
````

Here is an example of what bookings array shall have
````
[
	{
		"id" : 1,
		"team_id" : "3830d13e-129b-11e7-a045-027f448976bd",
		"hub_id" : "d9d4b51a-0f9d-11e7-98e5-3ca9f401bf34",
		"duration" : 225,
		"start_time" : "2017-04-04 12:00:00",
		"end_time" : "2017-04-04 15:45:00",
		"organization_name" : "Wizquest Labs Pvt. Ltd.",
		"user_id" : "382c4790-129b-11e7-a045-027f448976bd",
		"event_id" : "tiv27pr9cgvfuomoihir589ivg",
		"user_name" : "Rishabh Jain"
	},
	{
		"id" : 2,
		"team_id" : "8e136f88-b7b2-11e6-8bc5-027f448976bd",
		"hub_id" : "28733592-b563-11e6-99d1-3ca9f401bf34",
		"duration" : 210,
		"start_time" : "2017-04-05 11:30:00",
		"end_time" : "2017-04-05 15:00:00",
		"organization_name" : "Intense IP Services",
		"user_id" : "185a33ca-b7b3-11e6-8bc5-027f448976bd",
		"event_id" : "ilja92b6deq7ku9f7uc33pabik",
		"user_name" : "Arun Arora"
	},
	...
]
````

This data represents meeting room bookings by various
users and teams.

Every row or item represent a unique booking.
`user_id` represents a user, `team_id` represents a team. Team is composed of users. A hub is
composed of teams. Rest of the columns are self explanatory.

***

## 2. Technology
This project uses NodeJS, [LoopBack](http://loopback.io) (built upon ExpressJS).

Loopback have concepts of Datasources and Connectors, Models and Relations among models, Remote Methods, Middlewares etc.

From the JSON example, following datasource, models and their relations are created.

### 2.1. Datasource
Loopback uses connectors to save data to a datasource.

~~For this assignment, **Memory connector** is used which uses numeric ids to uniquely identify data rows.~~

For this assignment, **MongoDB connector** is used which uses 24 characters long alphanumeric ids to uniquely identify data rows.

~~#### 2.1.1. Why any database connector (to connect to database like MongoDB/MySql) is not used?~~
~~To prevent installation of MongoDB or other databases on local machine and to reduce the installation steps to test the API endpoints.~~

### 2.2. Models
Models are the core business models and correspond to tables/collection in a datasource like MySql/MongoDB.

 1. **AppUser:** Defines the user of the app
 1. **Organization:** Defines the organization to which user belongs
 1. **Hub:** Defines the Hub in which a user of an organization is placed. Hub is identified by its location like city
 1. **OrganizationHubTeam:** Defines a team of users (a segment of users) of a particular organization in a particular hub
 1. **Booking:** Defines the meeting room booking booked by a user

### 2.3. Relations

 1. **AppUser** belongs to **Organization**
 1. **AppUser** belongs to **OrganizationHubTeam**
 1. **OrganizationHubTeam** belongs to **Hub**
 1. **OrganizationHubTeam** belongs to **Organization**
 1. **Booking** belongs to **AppUser**

### 2.4. More relations (Not Used)
These relationships are logically required as per the app but they are not needed to meet the outcome

 1. **Hub** has many **OrganizationHubTeam**
 1. **OrganizationHubTeam** has many **AppUser**

***

## 3. Setup
Here are the one time steps to setup and run the app:


### 3.1. Tools Installation
Depending upon your operating system (Windows or MacOS) and bit version (32 bit or 64 bit), install the following tools:

 1. [Git](https://git-scm.com/): Git is a free and open source distributed version control system. Git is also used by npm and bower package managers to install project's development and code dependencies.
 1. [Node](https://nodejs.org/en/): Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node also contains **npm** package manager which is used to download project dependencies.
 1. [MongoDB](https://docs.mongodb.com/manual/installation/): This assignment uses MongoDB as datasource.

### 3.2. Steps
 1. Execute `git clone git@bitbucket.org:riteshjagga/booking-app.git` in terminal to clone booking-app repository.
 1. Execute `cd booking-app` to go inside the directory.
 1. Execute `npm install` to install project dependencies.
 1. Execute `node .` to run the app.
 1. When app runs, a number of boot scripts are executed sequentially to add following initial data:
     * 3 organizations having 10 users each are created.
     * 7 hubs are created out of which Delhi and Bangalore hubs are used for team formation.
     * Every organization users are divided into 2 teams having 5 user each for Delhi and Bangalore hub teams.
     * 10 bookings are created for every user. `3 organizations * 10 users each organization * 10 bookings each user = 300 total bookings`.
     * Booking start date-time is NOW (i.e. the time when script is executed). Subsequent bookings start date-time is shifted by an HOUR from previous booking. Duration of every booking is ONE HOUR.
     * You may explore terminal logs to know more.

***

## 4. Code Review
 1. Go to **Source** from the side menu to explore source.
 1. For asynchronous programming, Loopback initially used callback mechanism but is being updated to use Promises. In my code, you will see mix of callback and promise based approaches for asynchronous tasks.
 1. Directories and files for your review

    * **boot-scripts-data:** This directory contains a number of boot scripts which are executed, upon starting the app, to add the initial data.
    * **models:** This directory contains `.json` and `.js` file for each model. For example, `Booking.json` defines the model and its properties and `Booking.js` defines the private methods, remote methods, REST API configuration methods.
    * **scripts:** This directory contains other scripts file like `Util.js` containing some utility methods which can be used in any script file.

 1. You may ignore following directories and files from your review as they are configuration/backup related:

    * Most of the **.json** files are configuration related.
    * **backup-scripts-backup** directory containing the backup of boot scripts.
    * **Client** directory which is not used in this project assignment.
    * Files having **.delete** at the end.

***

## 5. Test
Once app is running successfully, API endpoints are exposed at this path: `http://localhost:3000/api/`

In order to execute below API endpoints, you'll need `ownerId` and `hubId`.

### 5.1. How to get `ownerId` and `hubId`?

 1. Open `http://localhost:3000/explorer` to open API explorer.
 1. Navigate to **AppUser > GET /AppUsers > Try it out! (button)** to get list of available users. You can copy id of one of the user from the list.
 1. Follow similar steps to get `hubId`.

### 5.2. API Endpoints

**Development Environment:**

Get bookings by owner:
````
http://localhost:3000/api/AppUsers/getBookings?userId=59c11fdc1762e70012a93a47&startDate=2017-09-01T00%3A00%3A00&endDate=2017-09-30T00%3A00%3A00
````

Get bookings by hub:
````
http://localhost:3000/api/Hubs/getBookings?hubId=59c11fe21762e70012a93a66&startDate=2017-09-01T00%3A00%3A00&endDate=2017-09-30T00%3A00%3A00

````

In both API endpoints, `startDate` and `endDate` are optional

 * Bookings are returned for duration >= `startDate` and <= `endDate`
 * If `startDate` is not defined, it is set to first day of current month
 * If `endDate` is not defined, it is set to last day of current month

### 5.3. Production environment
App has been deployed to heroku server.

In order to access API endpoints on heroku server, replace `http://localhost:3000` with `https://rj-booking-app.herokuapp.com`.

***


## 6. Feedback/Issue

### 6.1. Issue: Port already used

This app uses port 3000. If you get port related issue that port is being used then edit port in `config.json` file, may be to 3001 or 3002, and run app again.

### 6.2. Feedback/Issue
For any feedback/issue, send an email at riteshjagga@gmail.com

***
